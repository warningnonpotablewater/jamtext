# THIS PIECE OF SOFTWARE IS NO LONGER DEVELOPED

Warning: this library is alpha-quality software, be cautious when
using it in production.

jamtext is a markup language based on JSON that extends it with the
following features:

* Any value's type can be annotated using a so-called "flavor".

```
student {
	"id": i32 46983
}
```

* Object members with no key are added to the list property called
  `_children`.

```
ol {
	"type": "a",

	li {"Dogs"},
	li {"Cats"},
	li {"Flowers"}
}
```
* Comment support.

```
12 # What a nice number
```

* Strings can have line breaks inside.

```
"hello
world"
```

* Arrays and objects can have trailing a trailing comma after the last
  member.

```
[
	9,
	4,
	3,
]
```

By being backwards-compatible with JSON, it allows for both
lightweight object notation and document representation.

## How to build

```
meson build
cd build
meson compile
```

## How to use

See the `examples` directory.

## How to test

See the `tests` directory.
