#include <stdio.h>
#include <string.h>
#include <jamtext.h>

#define print(...)\
	if (fprintf(output, __VA_ARGS__) < 0) {\
		die("out of memory");\
	}

void serialize(FILE *output, struct jam_value *input);

void die(const char *last_words) {
	fprintf(stderr, "jtxt2xml: error: %s\n", last_words);
	exit(1);
}

void serialize_attribute_key(FILE *output, char *str) {
	for (size_t i = 0; str[i] != '\0'; i++) {
		switch (str[i]) {
		case ' ': // FALLTHROUGH
		case '<': // FALLTHROUGH
		case '>': // FALLTHROUGH
		case '"': // FALLTHROUGH
		case '=':
			die("illegal character in attribute key/node type");
		default:
			print("%c", str[i]);
			
			break;
		}
	}
}

void serialize_attribute_value(FILE *output, char *str) {
	print("\"");
	
	for (size_t i = 0; str[i] != '\0'; i++) {
		switch (str[i]) {
		case '\n': // FALLTHROUGH
		case '\r': // FALLTHROUGH
		case '"':
			die("illegal character in attribute value");
		default:
			print("%c", str[i]);
			
			break;
		}
	}
	
	print("\"");
}

void serialize_text(FILE *output, char *str) {
	for (size_t i = 0; str[i] != '\0'; i++) {
		switch (str[i]) {
		case '"':
			print("&quot;");
			
			break;
		case '\'':
			print("&apos;");
			
			break;
		case '<':
			print("&lt;");
			
			break;
		case '>':
			print("&gt;");
			
			break;
		case '&':
			print("&amp;");
			
			break;
		default:
			print("%c", str[i]);
			
			break;
		}
	}
}

void serialize_node(FILE *output, struct jam_value *input) {
	struct jam_value *children = jam_object_get(input, "children");
	
	print("<");
	serialize_attribute_key(output, input->flavor);
	
	struct jam_object *attribute = NULL;
	
	jam_object_foreach (attribute, input) {
		if (strcmp(attribute->key, "children") == 0) {
			continue;
		}
		
		if (attribute->value->type != JAM_VALUE_STRING) {
			die("illegal attribute value type");
		}
		
		print(" ");
		
		serialize_attribute_key(output, attribute->key);
		print("=");
		serialize_attribute_value(output,
			attribute->value->contents.as_string
		);
	}
	
	if (children == NULL) {
		print(" /");
	}
	print(">");
	
	if (children != NULL) {
		if (children->type != JAM_VALUE_LIST) {
			die("illegal children container type");
		}
		
		size_t index = 0;
		struct jam_list *child = NULL;
		
		jam_list_foreach (index, child, children) {
			serialize(output, child->value);
		}
		
		print("</");
		serialize_attribute_key(output, input->flavor);
		print(">");
	}
}

void serialize(FILE *output, struct jam_value *input) {
	switch (input->type) {
	case JAM_VALUE_STRING:
		serialize_text(output, input->contents.as_string);
		
		break;
	case JAM_VALUE_OBJECT:
		serialize_node(output, input);
		
		break;
	default:
		die("illegal object type");
	}
}

int main(int argc, char **argv) {
	if (argc != 3) {
		die("invalid number of arguments");
	}
	
	FILE *input = fopen(argv[1], "r");
	
	if (input == NULL) {
		die("couldn't open the input file");
	}
	
	FILE *output = fopen(argv[2], "w");
	
	if (output == NULL) {
		die("couldn't open the output file");
	}
	
	struct jam_deserializer *deserializer = jam_deserializer_new(input);
	
	if (deserializer == NULL) {
		die("out of memory");
	}
	
	struct jam_value *value = jam_deserializer_run(deserializer);
	
	if (value == NULL) {
		die(jam_deserializer_get_error(deserializer));
	}
	
	serialize(output, value);
	
	return 0;
}
