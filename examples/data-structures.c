#include <jamtext.h>

int main() {
	struct jam_value *list = jam_list_new("",
		jam_number_new("", 6),
		jam_number_new("", 9),
	NULL);
	
	if (list == NULL) {
		return 1;
	}
	
	jam_list_push(list, jam_number_new("", 0));
	
	printf("%lf\n", jam_list_get(list, 0)->contents.as_number);
	
	size_t index = 0;
	struct jam_list *element = NULL;
	
	jam_list_foreach (index, element, list) {
		printf("%zu: %lf\n", index, element->value->contents.as_number);
	}
	
	struct jam_value *object = jam_object_new("",
		"six", jam_number_new("", 6),
		"nine", jam_number_new("", 9),
	NULL);
	
	if (object == NULL) {
		return 1;
	}
	
	jam_object_set(object, "zero", jam_number_new("", 0));
	
	printf("%lf\n", jam_object_get(object, "six")->contents.as_number);
	
	struct jam_object *element1 = NULL;
	
	jam_object_foreach (element1, object) {
		printf("%s: %lf\n", element1->key, element1->value->contents.as_number);
	}
	
	return 0;
}
