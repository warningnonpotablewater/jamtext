#include <jamtext.h>

int main() {
	struct jam_deserializer *deserializer = jam_deserializer_new(stdin);
	
	if (deserializer == NULL) {
		fprintf(stderr, "error: out of memory");
		
		return 1;
	}
	
	struct jam_value *value = jam_deserializer_run(deserializer);
	
	if (value == NULL) {
		size_t
			line = jam_deserializer_get_line(deserializer),
			column = jam_deserializer_get_column(deserializer);
		
		const char *error = jam_deserializer_get_error(deserializer);
		
		fprintf(stderr, "error: %zu: %zu: %s\n", line, column, error);
		
		return 1;
	}
	
	struct jam_serializer *serializer = jam_serializer_new(stdout);
	
	if (serializer == NULL) {
		fprintf(stderr, "error: out of memory");
		
		return 1;
	}
	
	bool success = jam_serializer_run(serializer, value);
	
	if (!success) {
		const char *error = jam_serializer_get_error(serializer);
		
		fprintf(stderr, "error: %s\n", error);
		
		return 1;
	}
	
	jam_free(value);
	free(deserializer);
	free(serializer);
	
	return 0;
}
