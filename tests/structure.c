#include <jamtext.h>
#include <string.h>

enum test_result {
	SHOULD_SUCCEED,
	SHOULD_FAIL
};

void test(char *name, char *input, enum test_result result) {
	struct jam_deserializer *deserializer =
		jam_deserializer_new_from_string(input);
	
	if (deserializer == NULL) {
		fprintf(stderr, "error: out of memory\n");
		
		exit(1);
	}
	
	struct jam_value *value = jam_deserializer_run(deserializer);
	
	if (
		(value == NULL && result == SHOULD_FAIL) ||
		(value != NULL && result == SHOULD_SUCCEED)
	) {
		printf("Test '%s' passed\n", name);
	} else {
		printf("Test '%s' failed\n", name);
	}
	
	jam_free(value);
	free(deserializer);
}

int main() {
	test(
		"duplicate value",
		"6 6",
		SHOULD_FAIL
	);
	
	test(
		"too big number",
		"1e999999",
		SHOULD_FAIL
	);
	test(
		"too small number",
		"-1e999999",
		SHOULD_FAIL
	);
	test(
		"invalid number",
		"0bama",
		SHOULD_FAIL
	);
	test(
		"hex number",
		"0xff",
		SHOULD_FAIL
	);
	
	test(
		"string with a line break",
		"\"a\nb\"",
		SHOULD_SUCCEED
	);
	
	test(
		"Unicode escape with bad digits",
		"\"\\uhhhh\"",
		SHOULD_FAIL
	);
	test(
		"Unicode escape with not enough digits",
		"\"\\u6\"",
		SHOULD_FAIL
	);
	
	test(
		"empty string",
		"\"\"",
		SHOULD_SUCCEED
	);
	test(
		"empty list",
		"[]",
		SHOULD_SUCCEED
	);
	test(
		"empty object",
		"{}",
		SHOULD_SUCCEED
	);
	
	test(
		"unclosed string",
		"\"",
		SHOULD_FAIL
	);
	test(
		"unclosed list",
		"[[[",
		SHOULD_FAIL
	);
	test(
		"unclosed object",
		"{{{",
		SHOULD_FAIL
	);
	test(
		"unopened list",
		"]]]",
		SHOULD_FAIL
	);
	test(
		"unopened object",
		"}}}",
		SHOULD_FAIL
	);
	
	test(
		"trailing comma in a list",
		"[6,]",
		SHOULD_SUCCEED
	);
	test(
		"trailing comma in an object",
		"{6,}",
		SHOULD_SUCCEED
	);
	
	test(
		"object key with a wrong type",
		"{6: 7}",
		SHOULD_FAIL
	);
	test(
		"duplicate object member",
		"{\"a\": 6, \"a\": 7}",
		SHOULD_FAIL
	);
	test(
		"object member with a missing key",
		"{: 6}",
		SHOULD_FAIL
	);
	test(
		"object member with a missing value",
		"{\"a\": }",
		SHOULD_FAIL
	);
	
	test(
		"flavor",
		"list []",
		SHOULD_SUCCEED
	);
	test(
		"double flavor",
		"list list []",
		SHOULD_FAIL
	);
	test(
		"flavor in a wrong place",
		"[] list",
		SHOULD_FAIL
	);
	test(
		"double flavor in a wrong place",
		"list [] list",
		SHOULD_FAIL
	);
	test(
		"flavor with a bad name",
		"null []",
		SHOULD_FAIL
	);
	test(
		"flavor with a suspicious name",
		"Null []",
		SHOULD_SUCCEED
	);
	
	test(
		"children",
		"{\"a\": 6, 7, \"b\": 8, 9}",
		SHOULD_SUCCEED
	);
	test(
		"children container with a wrong type",
		"{\"_children\": null, 6}",
		SHOULD_FAIL
	);
	
	test(
		"comment",
		"12 # What a nice number",
		SHOULD_SUCCEED
	);
	test(
		"comment inside string",
		"\"a#b\"",
		SHOULD_SUCCEED
	);
	
	return 0;
}
