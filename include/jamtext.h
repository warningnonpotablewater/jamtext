#ifndef JAMTEXT_H
#define JAMTEXT_H

#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

extern const int
	JAM_VERSION_MAJOR,
	JAM_VERSION_MINOR,
	JAM_VERSION_PATCH;

extern size_t jam_nesting_limit;

struct jam_list {
	struct jam_value *value;
	
	size_t length;
	
	struct jam_list *next;
	struct jam_list *last;
};

struct jam_object {
	char *key;
	struct jam_value *value;
	
	size_t key_length;
	
	struct jam_object *next;
};

enum jam_value_type {
	JAM_VALUE_NULL,
	JAM_VALUE_BOOL,
	JAM_VALUE_NUMBER,
	JAM_VALUE_STRING,
	JAM_VALUE_LIST,
	JAM_VALUE_OBJECT
};

union jam_value_contents {
	bool as_bool;
	double as_number;
	char *as_string;
	struct jam_list *as_list;
	struct jam_object *as_object;
};

struct jam_value {
	enum jam_value_type type;
	char *flavor;
	
	union jam_value_contents contents;
	void *user_data;
};

void jam_free(struct jam_value *);

struct jam_value *jam_null_new(const char *);
struct jam_value *jam_bool_new(const char *, bool);
struct jam_value *jam_number_new(const char *, double);
struct jam_value *jam_string_new(const char *, const char *);
struct jam_value *jam_list_new(const char *, ...);
struct jam_value *jam_object_new(const char *, ...);

struct jam_value *jam_list_get(const struct jam_value *, size_t);
bool jam_list_push(struct jam_value *, struct jam_value *);

struct jam_value *jam_object_get(
	const struct jam_value *,
	const char *
);
bool jam_object_set(
	struct jam_value *,
	const char *,
	struct jam_value *
);

#define jam_list_foreach(index, element, value)\
	for (\
		index = 0, element = value->contents.as_list;\
		element != NULL;\
		index++, element = element->next\
	)
#define jam_object_foreach(element, value)\
	for (\
		element = value->contents.as_object;\
		element != NULL;\
		element = element->next\
	)

struct jam_serializer;

struct jam_serializer *jam_serializer_new(FILE *);
struct jam_serializer *jam_serializer_new_to_string(char **);

bool jam_serializer_run(
	struct jam_serializer *,
	const struct jam_value *
);

const char *jam_serializer_get_error(struct jam_serializer *);

struct jam_deserializer;

struct jam_deserializer *jam_deserializer_new(FILE *);
struct jam_deserializer *jam_deserializer_new_from_buffer(
	const char *,
	size_t
);
struct jam_deserializer *jam_deserializer_new_from_string(const char *);

struct jam_value *jam_deserializer_run(struct jam_deserializer *);

const char *jam_deserializer_get_error(struct jam_deserializer *);
size_t jam_deserializer_get_line(struct jam_deserializer *);
size_t jam_deserializer_get_column(struct jam_deserializer *);
size_t jam_deserializer_get_cursor(struct jam_deserializer *);

#endif
