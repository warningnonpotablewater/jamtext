#include <inttypes.h>
#include <math.h>

#include "jamtext.h"
#include "jamtext-internal.h"

#define jam_print(...) if (printfn(stream, __VA_ARGS__) < 0) {\
	return false;\
}

static const int PRECISION = 15;

static int count_digits(double n) {
	return floor(log10(n) + 1);
}

bool jam_print_number(
	jam_printfn_t printfn,
	void *stream,
	double number
) {
	if (signbit(number)) {
		jam_print("-");
		
		number = -number;
	}
	
	if (isnan(number)) {
		jam_print("nan");
		
		return true;
	}
	if (!isfinite(number)) {
		jam_print("inf");
		
		return true;
	}
	
	uint64_t integral = 0, decimal = 0;
	int exponent = 0;
	
	if (number > 9007199254740991) {
		exponent = count_digits(number) - 1;
		
		number = number / pow(10, exponent);
	} else if (number < 1 / pow(10, PRECISION)) {
		exponent = count_digits(number) - 1;
		
		number = number * pow(10, -exponent);
		
		if (!isfinite(number)) {
			number = 0;
		}
	}
	
	number = round(pow(10, PRECISION) * number) / pow(10, PRECISION);
	
	if (number == 0) {
		jam_print("0");
		
		return true;
	}
	
	integral = number;
	decimal = round(fmod(number, 1) * pow(10, PRECISION));
	
	jam_print("%" PRIu64, integral);
	
	if (decimal != 0) {
		jam_print(".");
		
		int decimal_length = count_digits(decimal);
		
		for (int i = 0; i < PRECISION - decimal_length; i++) {
			jam_print("0");
		}
		
		jam_print("%" PRIu64, decimal);
	}
	
	if (exponent != 0) {
		jam_print("e%i", exponent);
	}
	
	return true;
}
