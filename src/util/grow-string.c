#include <stdint.h>
#include <stdlib.h>

void jam_grow_string(char **str, size_t *size, size_t *used, size_t amount) {
	if (*size - *used >= amount) {
		return;
	}
	
	*size *= 2;
	*size += amount;
	
	char *new_str = realloc(*str, (*size + 1) * sizeof(char));
	
	if (new_str == NULL) {
		free(*str);
	}
	
	*str = new_str;
}
